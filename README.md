# cwinwebsite #

This is a web site, created by someone, for Jimmy. 

It lives in an AWS S3 bucket called `cwinwebsite`, which is a CloudFront-enabled bucket. Some Records in the `cwintech.com` HostedZone (Route53) point to the cloud front instance, and the site is served!